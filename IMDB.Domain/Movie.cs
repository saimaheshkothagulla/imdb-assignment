﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMDB.Domain
{
    public class Movie
    {
        public string Title { get; set; }
        public int YearOfRelease { get; set; }
        public string Plot { get; set; }
        public List<Actor> Actors { get; set; }
        public Producer Producer { get; set; }
        public Movie(string name, int yearOfRelease, string plot, List<Actor> actors, Producer producer)
        {
            Title = name;
            YearOfRelease = yearOfRelease;
            Plot = plot;
            Actors = actors;
            Producer = producer;
        }
    }
}
