﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMDB.Domain
{
    public class Actor
    {
        public string Name { set; get; }
        public string DateOfBirth { set; get; }
        public Actor(String name, string dob)
        {
            Name = name;
            DateOfBirth = dob;
        }
    }
}
