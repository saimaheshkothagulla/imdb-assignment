﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMDB.Domain;

namespace IMDB.Repository
{
    public class ActorRepo
    {
        public List<Actor> actors;
          
        public ActorRepo()
        {
            actors = new List<Actor>();
        }
       public void Add(Actor actor)
        {
            actors.Add(actor);
        }
        public List<Actor> GetList()
        {
            return actors.ToList();
        }
    }
}
