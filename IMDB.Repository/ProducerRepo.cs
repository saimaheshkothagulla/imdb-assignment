﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMDB.Domain;
namespace IMDB.Repository
{
    public class ProducerRepo
    {
        public List<Producer> producers;
        public ProducerRepo()
        {
            producers = new List<Producer>();
        }
        public void Add(Producer producer)
        {
            producers.Add(producer);
        }
        public List<Producer> GetList()
        {
            return producers.ToList();
        }
    }
}
