﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
namespace IMDB
{
    class Program
    {
        
        public static void Main(string[] args)
        {
            IMDBService imdb = new IMDBService();
           


            int choice;
            while (true)
            {
                string options = "enter your Choice  :\n1.list movies\n2.add movies\n3.add actors\n4.add producers\n5.delete movie\n6.exit";
                Console.WriteLine(options);
                choice = Convert.ToInt32(Console.ReadLine());
                switch(choice)
                {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        try
                        {
                            string actorName;
                            string actorDOB;
                           
                            Console.Write("enter Name of actor: ");
                            actorName = Console.ReadLine().Trim();
                            

                            while (string.IsNullOrEmpty(actorName) || actorName.Length < 5)
                            {
                                Console.WriteLine("your enterted name is invalid.name should not be null/empty/ and length should atleast 5 charaters.");
                                Console.Write("please ,enter Name of actor: ");
                                actorName = Console.ReadLine();
                            }

                            Console.Write("enter date of birth :");
                           
                               
                            
                            actorDOB = Convert.ToDateTime(DateTime.ParseExact(Console.ReadLine().Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture)).ToString("dd-MM-yyyy"); ;



                            imdb.AddActor(actorName, actorDOB);
                            Console.WriteLine("actor list..");
                            foreach(var actor in imdb.GetActors())
                            {
                                Console.WriteLine(actor.Name + " " + actor.DateOfBirth);
                            }

                        }
                        catch(Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                      
                        break;
                    case 4:
                        try
                        {
                            string producerName;
                            string producerDOB;

                            Console.Write("enter Name of Producer: ");
                            producerName = Console.ReadLine().Trim();


                            while (string.IsNullOrEmpty(producerName) || producerName.Length < 5)
                            {
                                Console.WriteLine("your enterted name is invalid.name should not be null/empty/ and length should atleast 5 charaters.");
                                Console.Write("please ,enter Name of actor: ");
                                producerName = Console.ReadLine();
                            }
                            Console.Write("enter date of birth : ");
                            producerDOB = Convert.ToDateTime(DateTime.ParseExact(Console.ReadLine().Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture)).ToString("dd-MM-yyyy"); ;
                            imdb.AddProducer(producerName, producerDOB);
                            Console.WriteLine("actor list..");
                            foreach (var producer in imdb.GetActors())
                            {
                                Console.WriteLine(producer.Name + " " + producer.DateOfBirth);
                            }

                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                        break;
                    case 5:
                        break;
                    case 6:
                        Environment.Exit(0);
                        break;
                    default:
                        Console.WriteLine("enter valid choice..");
                        break;
                        

                        
                }
            }

                               

        }
    }
}

