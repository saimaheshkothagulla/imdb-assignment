﻿using System;
using System.Collections.Generic;
using System.Text;
using IMDB.Domain;
using IMDB.Repository;
using System.Linq;

namespace IMDB
{
    public class IMDBService
    {

        
        public ActorRepo actorRepository;
        public ProducerRepo producerRepository;
        public MovieRepo movieRepository;
        public IMDBService()
        {

      
            actorRepository = new ActorRepo();
            producerRepository = new ProducerRepo();
            movieRepository = new MovieRepo();
        }

       
        public void AddActor(String name, string dateOfBirth)
        {
            var actor = new Actor(name, dateOfBirth);
            actorRepository.Add(actor);
        }
        public List<Actor> GetActors()
        {
            return actorRepository.GetList();
        }
        public void AddProducer(String name, string dateOfBirth)
        {
            var producer = new Producer(name, dateOfBirth);
            producerRepository.Add(producer);
        }
        public List<Producer> GetProducers()
        {
            return producerRepository.GetList();
        }







    }
}
